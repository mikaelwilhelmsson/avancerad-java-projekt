package Labbuppgifter;

import java.util.Scanner;

// Concret-product som implementerar Superklassen / Product

public class Spelare implements Motstandare {

    public static String spelarVal() {
        String val = "";
        Scanner scanner = new Scanner(System.in);
        System.out.println("Välj ditt drag: ");
        System.out.println("""
                1.Sten
                2.Sax
                3.Påse
                """);
        int svar = scanner.nextInt();
        switch (svar) {
            case 1 -> val = "Sten";
            case 2 -> val = "Sax";
            case 3 -> val = "Påse";
        }
        System.out.println("Du valde: " + val);
        return val;
    }


    @Override
    public String Turn() {
        return spelarVal();
    }

    @Override
    public String namn() {
        return null;
    }
}
