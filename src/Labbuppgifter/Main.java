package Labbuppgifter;

import java.util.*;

//Simple factory pattern - Creator, Concrete Creators samt Product.

public class Main {

    public static void main(String[] args) {

        String namn;
        Scanner scanner = new Scanner(System.in);
        SpelarFabrik spelarFabrik = new SpelarFabrik();


        List<Motstandare> list1 = List.of(
                spelarFabrik.getSpelare("Slumpis"),
                spelarFabrik.getSpelare("Vokalis"),
                spelarFabrik.getSpelare("Klockis")
        );


        System.out.println("Välj ditt spelarnamn: ");
        namn = scanner.nextLine();
        System.out.println("Välkommen " + namn + "!");
        boolean spel = true;
        while (spel) {
            Random random = new Random();
            Motstandare motstandare = list1.get(random.nextInt(list1.size()));
            System.out.println("---------");
            System.out.println("Du möter: " + motstandare.namn());
            Labbuppgifter.Spelare.spelarVal();
            System.out.println(motstandare.Turn());
        }
    }
}







