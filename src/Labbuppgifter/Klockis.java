package Labbuppgifter;

import java.time.LocalDateTime;
import java.util.List;

// Concret-product som implementerar Superklassen / Product

public class Klockis implements Motstandare {

    static String getValAvKlockis() {

        List<String> val = List.of("Sten", "Sax", "Påse");
        LocalDateTime date = LocalDateTime.now();
        int valKlockis = date.getMinute();

        String klockisVal;
        if (valKlockis % 3 == 0) {
            klockisVal = val.get(0);
        } else if (valKlockis % 2 == 0) {
            klockisVal = val.get(1);
        } else {
            klockisVal = val.get(2);
        }
        return "Klockis valde: " + klockisVal;

    }

    @Override
    public String Turn() {
        return getValAvKlockis();
    }

    @Override
    public String namn() {
        return "Klockis";
    }
}

