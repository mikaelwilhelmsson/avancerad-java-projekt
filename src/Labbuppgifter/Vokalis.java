package Labbuppgifter;

import java.util.regex.Pattern;

// Concret-product som implementerar Superklassen / Product

public class Vokalis implements Motstandare{

    String getValAvVokalis (String namnPaSpelare) {

        Pattern pattern1 = Pattern.compile("([aeiouyåäö])+");
        Pattern pattern2 = Pattern.compile("([aeiouyåäö]){2,}");

        String vokalisVal;

        if (pattern1.matcher(namnPaSpelare).find()) {
            vokalisVal = "Sten";
        } else if (pattern2.matcher(namnPaSpelare).find()) {
            vokalisVal = "Sax";
        } else {
            vokalisVal = "Påse";
        }
        return "Vokalis valde: " + vokalisVal;
    }

    @Override
    public String Turn() {
        return getValAvVokalis("");
    }

    @Override
    public String namn() {
        return "Vokalis";
    }

}
