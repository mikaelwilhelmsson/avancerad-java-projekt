package Labbuppgifter;

//Creator - instansierar objekt

public class SpelarFabrik {

    public Motstandare getSpelare(String SpelarTyp) {

        if (SpelarTyp == null) {
            return null;
        }
        if (SpelarTyp.equalsIgnoreCase("Slumpis")) {
            return new Slumpis();
        }
        if (SpelarTyp.equalsIgnoreCase("Klockis")) {
            return new Klockis();
        }
        if (SpelarTyp.equalsIgnoreCase("Vokalis")) {
            return new Vokalis();
        }
        return null;

    }
}
