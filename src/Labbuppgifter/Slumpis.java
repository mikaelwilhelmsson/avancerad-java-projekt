package Labbuppgifter;

import java.util.List;
import java.util.Random;

// Concret-product som implementerar Superklassen / Product

public class Slumpis implements Motstandare {

    public String getValAvSlumpis() {

        List<String> val = List.of("Sten", "Sax", "Påse");
        Random random = new Random();
        String slumpisVal = val.get(random.nextInt(val.size()));

        return "Slumpis valde: " + slumpisVal;
    }
    @Override
    public String Turn() {
            return getValAvSlumpis();
        }

    @Override
    public String namn() {
        return "Slumpis";
    }
}


